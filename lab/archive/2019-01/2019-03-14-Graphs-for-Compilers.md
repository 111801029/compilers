# Graphs for compilers

In this assignment, we are going to represent a graph using an ml
structure.  Implement a standard ml structure/functor to represent a
graph. Graphs are used in compilers to represent the control flow
graphs of programs so this should be tailor made for such application.

Maybe you can start with implementing some structure of the following
kind.


```
signature GRAPH = sig

	type node
	val new     : ()   -> node
	
	
	
	val addEdge : (node,node) -> ()

	(* addEdge (a,b) should add and edge starting from a to b *)
	
	val succ    : node -> node list
	val pred    : node -> node list 
	
end

```

For application to compilers you would need to have a way to label the
nodes. In the case of programs, the labels could be the actual
instructions. The simplest way to keep track of labels is to keep it
as a map from the nodes to labels. So the graph signature can be
required to have inside it a structure that captures node maps, and
while we are at it, node sets.

This graph structure will be used in the subsequent assignments where
we wish to do dataflow analysis.


