#+TITLE: Lab 0: Preparation

This assignment is the preparatory step in the building of the
compiler. You need to

1. Create a repository with the following files.

   - ~README.md~  :: That contains the details like name and roll no
   - ~CHANGELOG.md~  :: The weekly changes that you made in the project as a summary.
   - ~tc.sml~ :: An sml program that when executed prints "hello world"
   - ~Makefile~ :: A basic make file with targets ~all~ and ~clean~.
      - ~make all~ :: should build the executable ~tc~ from ~tc.sml~
      - ~make clean~ :: remove all generated/intermediate files from the repository
           (in this case only the executable file ~tc~)

2. Host the repository on gitlab.

3. Give read access the TA's and the instructors of the repository on gitlab.


For more details read the ~Readme.org~ file that is present in this
directory.
