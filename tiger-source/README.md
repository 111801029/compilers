# Source file for tiger programs.

This directory contains sample code written in tiger language. These
code may be used to test the tiger compiler that you have written.
The sample coded is arranged in the sub-directories as follows

1. The `constructs` directory contain programs that make use of a
   single illustrative construct of the tiger language. These programs
   are typically useful for testing the parser/lexer.

2. The `programs` directory contains larger examples and more
   meaningful examples. They can serve as tests programs to your
   compiler.
